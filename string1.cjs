// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 

function string1(str1){
    if(typeof str1 == "string"){
        if (str1.includes('$')){
            var str2  = str1;
            if(str2.includes(',')){
                str2 = str1.replace(",","")
            }
            str2 = str2.replace("$","")
            const num = Number(str2)
            return num
        }
        else{
            return 0
        }
    }
    else{
        return 0
    }
}

module.exports = string1;




