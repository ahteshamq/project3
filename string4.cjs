// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function string4(obj) {

    if(obj && Object.keys(obj).length == 0){
        return []
    }
    if(typeof obj === 'object'){
        const arr = []
        for (let value in obj){
            const temp = obj[value].toLowerCase()
            temp1 = temp[0].toUpperCase()+temp.slice(1)
            arr.push(temp1)
        }
        return arr.join(" ")
    }
    return []
}
module.exports = string4;